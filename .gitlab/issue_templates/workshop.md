# Welcome to the Congregate Onboarding Workshop

## Workshop pre-work

*This should be completed prior to the workshop if attending live in-person*

1. [ ] Download and install `docker`, `podman`, or [any other application that can run docker containers](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop). Make sure that your system has at least 8GB of memory available to run two GitLab instances as the migration source and destination systems. Depending on the tool you use you may need to edit available memory on the Virtual Machine if you are running the workshop on a MacOS or Microsoft Windows operating system.
1. [ ] Grab the workshop repo and start the applications.
    - Run `git clone git@gitlab.com:gitlab-com/customer-success/professional-services-group/global-practice-development/migration/congregate-onboarding-workshop.git` to get this repo to your local
    - Navigate into the repo directory using `cd congregate-onboarding-workshop`
    - Start the source, destination and congregate containers using `docker-compose up` and leave running in terminal window.
      - Depending on resources it might take several minutes to download the docker images and start (`healthy`) container instances
      - You may encounter issues logging into both your source and destination instances at the same time.  One workaround is to use two different browsers, and another is to use an incognito window for one of them.
    - From browser, login to instances (username: `root`, Password found in the [docker compose file](/docker-compose.yml#L13))
      - `127.0.0.1:8080` for **gitlab-src**
      - `127.0.0.1:8081` for **gitlab-dest**
    - Generate a personal access token, with `api` scope privileges, for each instance by clicking *User Icon (top right) > Preferences > Access Tokens > Generate Access Token*. Copy this to a text file or leave this page open.

## Workshop

1. [ ] Congregate List
    - Copy the [congregate.conf.template](congregate.conf.template) from the local repo to the congregate container:
        - `docker cp congregate.conf.template congregate:/opt/congregate/data/congregate.conf`
    - To get into congregate:
        - Run `docker-compose exec congregate /bin/bash` to connect to the congregate container.
    - We need to base64 encode the tokens we generated in the :point_up: step. To do this run `congregate obfuscate`. This will prompt you to paste the token from the source system. It will output the encoded token to the terminal output or clipboard.
    - Edit the `data/congregate.conf` file and paste the encoded tokens into the `src_access_token` field for the source i.e. `dest_access_token` for the destination instance.
    - Once we have our tokens setup we can run `congregate list`
    - Watch the output log stream to ensure listing is happening without error. This might take a few minutes .

1. [ ] Congregate Stage
    - Let's look at the listed data to find a repo or group to migrate:
        - `less data/groups.json` or `less data/projects.json`
    - Take note of the group ID that you want to migrate.
        - `congregate stage-groups <space delimited list of group-ids> --commit` or
        - `congregate stage-projects <space delimited list of project-ids> --commit`

1. [ ] Enable project export via UI on the **gitlab-src** admin screen on `127.0.0.1:8080/admin`
    - Go to _Admin Area -> Settings -> General -> Import and export settings -> Project export_
    - Enable and **Save changes**

1. [ ] Enable GitLab exports via UI on the **gitlab-src** and **gitlab-dest** admin screen on `127.0.0.1:8080/admin` i.e. `127.0.0.1:8081/admin`
    - Go to _Admin Area -> Settings -> General -> Import and export settings -> Import Sources_
    - Enable **GitLab export** and **Save changes**

1. [ ] Congregate Migrate
    - Go to the **gitlab-src** admin screen on `127.0.0.1:8080/admin`
    - Take note of the number of groups, projects and users on the dashboard
    - From the congregate container
        - Dry-run `congregate migrate`
        - Review the output of the dry-run
        - Run `congregate migrate --commit`

1. [ ] Validation
    - Go to the **gitlab-dest** admin screen on `127.0.0.1:8081/admin` and watch the number of projects, users, and groups increment.
    - Click on the number of projects to see the list of projects.
    - Click on the most recently created project.
    - Once it finishes importing, spot check the number of branches, commits, MRs and users.
    - [ ] Post a screenshot of the **gitlab-src** and **gitlab-dest** instance that shows the project data has been migrated
    - [ ] Attach the logs from `/opt/congregate/data/logs/` to show the output of the successful congregate migration run.
    - [ ] Return to GitLab University to provide a link to this issue and your user ID. We will administer the grade for this workshop in there.

/label ~"workshop::doing"
